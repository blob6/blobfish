
import json

from django.test import TestCase
from rest_framework.test import APIClient

from game.models import User

class APITestCase(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.username = 'sil2ob'
        cls.wallet = 'mywallet'
        User.objects.create(username=cls.username, wallet=cls.wallet)
        super(APITestCase, cls).setUpClass()


    def setUp(self) -> None:
        self.APIClient = APIClient()

    def test_create_user_NOK(self) -> None:
        request = self.APIClient.post('/api/user', {
            'username': self.username,
            'wallet': self.wallet
        })
        response = json.loads(request.content)

        self.assertEqual(request.status_code, 200)
        self.assertEqual(response['error'], 'user already exist with this wallet and username')

    def test_create_user_OK(self) -> None:
        request = self.APIClient.post('/api/user', {
            'username': "siloob",
            'wallet': self.wallet
        })

        response = json.loads(request.content)

        self.assertEqual(request.status_code, 200)
        self.assertEqual(response['id'], 2)

    def test_check_user_OK(self) -> None:
        request = self.APIClient.get(f'/api/user?username={self.username}&wallet={self.wallet}')

        response = json.loads(request.content)

        self.assertEqual(request.status_code, 200)
        self.assertEqual(response['id'], 1)

    def test_check_user_NOK(self) -> None:
        request = self.APIClient.get(f'/api/user?username=bahtiens&wallet={self.wallet}')

        response = json.loads(request.content)

        self.assertEqual(request.status_code, 200)
        self.assertEqual(response['error'], 'User does not exist')