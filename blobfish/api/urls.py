from rest_framework import routers
from django.urls import include, path

from api.views import UserView

urlpatterns = [
    path('user', UserView.as_view())
]