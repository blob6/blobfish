from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets

from game.models import User

# Create your views here.
class UserView(APIView):
    
    def post(self, request) -> Response:
        user, created = User.objects.get_or_create(username=request.data['username'], wallet=request.data['wallet'])
        if created:
            response = Response({
                'id': user.id
            })
        else :
            response = Response({
                'error': 'user already exist with this wallet and username'
            })

        return response

    def get(self, request) -> Response:
        try:
            user = User.objects.get(username=request.query_params['username'], wallet=request.query_params['wallet'])
            response = Response({
                'id': user.id
            })
        except User.DoesNotExist:
            response = Response({
                'error': 'User does not exist'
            })

        return response