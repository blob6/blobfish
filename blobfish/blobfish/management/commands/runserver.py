from django.core.management import call_command
from django.contrib.staticfiles.management.commands.runserver import Command as StaticfilesRunserverCommand

from game.gamethread import GameThread

class Command(StaticfilesRunserverCommand):
    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--flush',
            action='store_true',
            help='flsuh db'
        )

    def handle(self, *args, **options):
        if options['flush']:
            call_command('flush', '--no-input')

        game_thread = GameThread()
        game_thread.daemon = True
        game_thread.start()

        super(Command, self).handle(*args, **options)