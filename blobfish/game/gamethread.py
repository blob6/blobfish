import django

from channels.layers import get_channel_layer

import threading
import asyncio
import time

async def send_map(id):
    channel_layer = get_channel_layer()
    await channel_layer.send(
        id,
        {"type": "send.map", "text":"0111010101"}
    )

class GameThread(threading.Thread):

    def run(self):
        django.setup()
        from game.models import User
        while True:
            time.sleep(3)
            users = User.objects.filter(id_socket__isnull=False).exclude(id_socket='')
            for user in users:
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                coroutine = send_map(user.id_socket)
                loop.run_until_complete(
                    coroutine
                )