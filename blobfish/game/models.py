from pyexpat import model
from django.db import models

# Create your models here.

class User(models.Model):
    id_socket = models.CharField(max_length=100)

    username = models.CharField(max_length=50, default='')
    wallet = models.CharField(max_length=50, default='')