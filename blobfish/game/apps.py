from django.apps import AppConfig

from game.gamethread import GameThread
class GameConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'game'