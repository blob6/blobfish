from channels.generic.websocket import AsyncJsonWebsocketConsumer
from websockets.utils import is_connected_msg

import json
class GameConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        await self.accept()

    async def receive(self, text_data=None):
        msg = ''
        try:
            data =  json.loads(text_data)
            if data['type'] == 'connect':
                msg = await is_connected_msg(data['id'], self.channel_name)
        except (KeyError, json.decoder.JSONDecodeError) as e:
            msg = {
                'connected': False,
                'message': 'Malformatted JSON'
            }
        
        await self.send_json(content=msg)

    async def send_map(self, event):
        await self.send_json(event['text'])

    async def disconnect(self, msg):
        print('disconnect')