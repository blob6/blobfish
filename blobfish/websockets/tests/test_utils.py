from django.test import TestCase

from unittest import mock
from websockets.utils import is_connected_msg

# Create your tests here.

class UtilsTest(TestCase):
    @mock.patch('websockets.utils.update_user_socket')
    async def test_message_connect_false(self, mock_update_user_socket):
        mock_update_user_socket.return_value = 0

        msg = await is_connected_msg(1,'okletsgo')
        assert mock_update_user_socket.called
        assert msg['connected'] == False
        assert msg['message'] == 'User does not exist'

    @mock.patch('websockets.utils.update_user_socket')
    async def test_message_connect_true(self, mock_update_user_socket):
        id = 1
        mock_update_user_socket.return_value = 1

        msg = await is_connected_msg(id,'okletsgo')
        assert mock_update_user_socket.called
        assert msg['connected'] == True
        assert msg['message'] == f'user {id} connected'