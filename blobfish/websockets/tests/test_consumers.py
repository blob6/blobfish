from django.test import TestCase

from unittest import mock
from channels.testing import WebsocketCommunicator
from websockets.consumers import GameConsumer

# Create your tests here.

class ConsumersTests(TestCase):

    @mock.patch('websockets.consumers.is_connected_msg')
    async def test_connect_false(self, mock_is_connected_msg):
        mock_is_connected_msg.return_value = {
            'connected': False,
            'message': 'User does not exist'
        }
        communicator = WebsocketCommunicator(GameConsumer.as_asgi(), "/ws/")
        connected, subprotocol = await communicator.connect()
        assert connected

        await communicator.send_json_to({"type": "connect", "id": 1})
        message = await communicator.receive_json_from()
        assert mock_is_connected_msg.called
        assert message['connected'] == False
        assert message['message'] == 'User does not exist'

    @mock.patch('websockets.consumers.is_connected_msg')
    async def test_connect_true(self, mock_is_connected_msg):
        id = 2
        mock_is_connected_msg.return_value = {
            "connected": True,
            "message": f'user {id} connected'
        }
        communicator = WebsocketCommunicator(GameConsumer.as_asgi(), "/ws/")
        connected, subprotocol = await communicator.connect()
        assert connected

        await communicator.send_json_to({"type": "connect", "id": id})
        message = await communicator.receive_json_from()
        assert mock_is_connected_msg.called
        assert message['connected'] == True
        assert message['message'] == f'user {id} connected'