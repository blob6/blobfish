from channels.db import database_sync_to_async
from game.models import User

async def is_connected_msg(id, channel_name):
    msg = {
        'connected': False,
        'message': 'User does not exist'
    }

    nb_lines = await update_user_socket(id, channel_name)
    if nb_lines > 0:
        msg = {
            "connected": True,
            "message": f'user {id} connected'
        }
    
    return msg

'''
    DB FUNCTIONS
'''

#return nb lines modified
@database_sync_to_async
def update_user_socket(id, channel_name):
    return User.objects.filter(id=id).update(id_socket=channel_name)
