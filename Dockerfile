# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY dockerlaunch.sh /code/dockerlaunch.sh
RUN chmod +x /code/dockerlaunch.sh
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
ENTRYPOINT ["sh", "/code/dockerlaunch.sh"]
